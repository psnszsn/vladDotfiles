#!/bin/bash


[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm git 
[[ -f /etc/debian_version ]] && sudo apt install git

cd ~/vladDotfiles/
stow --no-folding -v git
