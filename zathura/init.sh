#!/bin/bash

[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm zathura zathura-pdf-mupdf 
[[ -f /etc/debian_version ]] && echo "debian"

cd ~/vladDotfiles/
stow --no-folding -v zathura 
