Hints.characters = `asdfgjk`

unmap('u')
unmap('F')

map('L', 'D') //forward
map('H', 'S') //back
map('u', 'X') //undo
map('K', 'R') //next tab
map('<Tab>', 'K')
map('J', 'E') //previous tab
map('<Shift-Tab>', 'J')


map('za', 'oh') //open url from history
map('o', 'go')
map('O', 't')
map('F', 'af')
map(';b', 'C')
map('<Ctrl-i>','<Alt-i>')
map('<Ctrl-s>','<Alt-s>')

map('<Ctrl-d>', 'd')
map('<Ctrl-u>', 'e')
map('d', 'x')
unmap('D')
unmap('S')
unmap('R')
unmap('E')

settings.tabsThreshold = 1000;

unmap('f', /youtube\.com/i)
mapkey('h', '-5 sec', function() {
    let ct = document.getElementsByTagName("video")[0].currentTime;
    document.getElementsByTagName("video")[0].fastSeek(ct - 5);
    console.log(ct);
}, {domain: /youtube.com|hbogo.ro/});
mapkey('l', '+5 sec', function() {
    let ct = document.getElementsByTagName("video")[0].currentTime;
    document.getElementsByTagName("video")[0].fastSeek(ct + 5);
    console.log(ct);
}, {domain: /youtube.com|hbogo.ro/});

