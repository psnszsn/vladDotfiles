set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_DATA_HOME "$HOME/.local/share"
set -gx XDG_CACHE_HOME "$HOME/.cache"

set -gx NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME/npm/npmrc"
set -gx PLATFORMIO_CORE_DIR "$XDG_DATA_HOME/platformio"
set -gx WINEPREFIX "$XDG_DATA_HOME/wineprefixes/default"
set -gx WEECHAT_HOME "$XDG_CONFIG_HOME"/weechat
set -gx STUDIO_PROPERTIES "$XDG_CONFIG_HOME"/android-studio/idea.properties
set -gx STUDIO_VM_OPTIONS "$XDG_CONFIG_HOME"/android-studio/idea.vmoptions
# set -gx ANDROID_SDK_ROOT "$XDG_DATA_HOME/android_sdk"
# set -gx ANDROID_SDK_HOME "$XDG_CONFIG_HOME"/android
# set -gx ANDROID_HOME "$HOME"/fjHome/Android/Sdk
# set -gx GRADLE_USER_HOME "$XDG_DATA_HOME/gradle"

set -gx _JAVA_OPTIONS "-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java -Duser.home=$HOME/fjHome"
set -gx DOCKER_CONFIG "$XDG_CONFIG_HOME"/docker
