#!/bin/bash


[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm ranger libcaca highlight atool poppler mediainfo w3m imlib2 zip unzip unrar
[[ -f /etc/debian_version ]] && sudo apt install ranger caca-utils highlight atool w3m poppler-utils mediainfo

cd ~/vladDotfiles/
stow --no-folding -v ranger
