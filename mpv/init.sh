#!/bin/bash


[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm mpv
[[ -f /etc/debian_version ]] && echo debian 


cd ~/vladDotfiles/
stow --no-folding -v mpv
