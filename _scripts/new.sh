#!/bin/bash

case "$1" in
	"bash")
		printf "#!/bin/bash\n" > $2
		chmod +x $2
		nvim $2
		;;
	*)
		echo "error..."
		exit 1
		;;
esac
