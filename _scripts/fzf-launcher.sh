#!/bin/bash

XDG_DATA_DIRS+=":/home/vlad/.local/share"

IFS=':'

APPZ=$(
for p in $XDG_DATA_DIRS; do
	[[ "$p" != */ ]] && p2="${p}/"
	[[ -d "$p2"applications ]] && fd .desktop "$p2"applications
done)

SEL=$(
echo $APPZ | fzf --preview 'grep -m 1 "^Name=" {} | tail -1 | sed "s/^Name=//" ' --preview-window down:1
)

echo $SEL
# swaymsg -t command exec launchd $SEL

swaymsg -t command exec gtk-launch $(basename $SEL)


# echo $APPZ | fzf | xargs grep '^Exec' | tail -1 | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^"//g' | sed 's/" *$//g'
