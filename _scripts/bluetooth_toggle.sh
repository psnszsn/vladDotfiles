#!/bin/bash

if bluetoothctl show | grep "Powered: no" ; then
    bluetoothctl power on
    notify-send -i network-wireless-full "Bluetooth enabled" "Your bluetooth adaptor has been enabled."
else
    bluetoothctl power off
    notify-send -i network-wireless-disconnected "Bluetooth disabled" "Your bluetooth adaptor has been disabled."
fi
