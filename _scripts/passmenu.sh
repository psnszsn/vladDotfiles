#!/usr/bin/env sh

DB="/home/vlad/documents/Passwords.kdbx"
PASS=""
while IFS= read -r line
do
    case "$line" in
        \D*)
            echo yes
            PASS=$( echo $line | cut -d' ' -f2)
            ;;
        \ERR*)
            echo err 
            notify-send "pinentry error"
            exit
            ;;
        *) echo no ;;
    esac
done << EOF
$(pinentry-gnome3 << EOFF
SETTITLE Pinentry for KP
SETPROMPT KeepassXC pass here
SETDESC pls
GETPIN
EOFF
)
EOF

keepassxc-cli open $DB << EOF
$PASS
ls
EOF
