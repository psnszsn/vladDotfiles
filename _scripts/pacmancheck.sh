#!/bin/bash
if pacman -Qi $@ > /dev/null ; then
  echo "The packages are installed."
else
  echo "The packages are not installed."
  sudo pacman -Sy --needed --noconfirm $@
fi
