#!/bin/bash
APP=$(swaymsg -t get_tree | jq -r '.nodes|.[]|select(.current_workspace)|..?|select(.nodes?==[])|"\(.id) > \(.name)"' | $@)

ID=${APP%% >*}
echo $ID
swaymsg "[con_id=${ID}] focus"
