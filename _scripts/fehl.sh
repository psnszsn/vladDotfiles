#!/bin/bash
if [ -z "$1" ]; then
    exit
fi

FPATH=$( readlink -f "$1" )
FNAME="$(basename "$FPATH")"
DPATH="$(dirname "$FPATH")"
# echo "DPATH = $DPATH /// $FPATH"
echo "imv -n $FNAME $DPATH"
imv -n "$FNAME" "$DPATH"
