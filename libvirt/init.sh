#!/bin/bash


[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm qemu libvirt ovmf virt-manager
[[ -f /etc/debian_version ]] && echo debian

cd ~/vladDotfiles/
stow --no-folding -v libvirt
