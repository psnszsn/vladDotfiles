#!/bin/sh

[[ -f /etc/arch-release ]] && pacmancheck.sh fish
[[ -f /etc/debian_version ]] && sudo apt install -y fish fortune exa fzf autojump fd-find


cd ~/vladDotfiles/
stow --no-folding -v fish
