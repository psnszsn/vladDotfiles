#!/bin/bash

[[ ":$PATH:" != *":/home/${USER}/vladDotfiles/_scripts:"* ]] && PATH="/home/${USER}/vladDotfiles/_scripts:${PATH}"

export STOW_DIR=/home/$USER/vladDotfiles
./stow/init.sh
./fish/init.sh
./sway/init.sh
./tmux/init.sh
./nvim/init.sh
