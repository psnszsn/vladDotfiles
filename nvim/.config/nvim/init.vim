  call plug#begin('~/.local/share/nvim/plugged')
  " Plug 'tyru/caw.vim'
  " Plug 'Shougo/context_filetype.vim'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-repeat'
  Plug 'tpope/vim-sensible'
  Plug 'tpope/vim-fugitive'
  " Plug 'tpope/vim-vinegar'
  
  Plug 'sheerun/vim-polyglot'
  Plug 'neoclide/jsonc.vim'

  Plug 'easymotion/vim-easymotion'
  Plug 'junegunn/fzf.vim'
  Plug 'lambdalisue/suda.vim'
  Plug 'Yggdroot/indentLine'
  Plug 'itchyny/lightline.vim'
  Plug 'vimwiki/vimwiki'
  Plug 'jiangmiao/auto-pairs'
  Plug 'andymass/vim-matchup'
  Plug 'unblevable/quick-scope'

  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'antoinemadec/coc-fzf', {'branch': 'release'}

  Plug 'honza/vim-snippets'
  " Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
  " Plug 'neovim/nvim-lsp'
  Plug 'chriskempson/base16-vim'
  Plug 'arzg/vim-colors-xcode'
  Plug 'arcticicestudio/nord-vim'
  Plug 'mcchrish/nnn.vim'
  " Plug 'justinmk/vim-dirvish'
call plug#end()

let g:xml_syntax_folding=1

filetype plugin on
syntax on
set ts=4 sts=4 sw=4 expandtab
set listchars=tab:▸\ ,eol:¬
" set clipboard+=unnamedplus
set inccommand=nosplit
set termguicolors
set number relativenumber
set hidden
set mouse=a
set undofile 
" set lazyredraw
let &showbreak = '↳ '
set linebreak
set ignorecase
set cpo+=n
set shell=/usr/bin/bash
set path+=**
set wildignore+=node_modules/*,target/*
" set wildignore+=**/node_modules/**
set conceallevel=0

let g:netrw_banner = 0
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

let g:csv_no_conceal = 1

let g:coc_fzf_preview = ''
let g:coc_fzf_opts = []


let $FZF_DEFAULT_COMMAND = 'fd --type file'

nnoremap ; :

" let mapleader ="\<Space>"
noremap <Space> <Nop> 
map <Space> <leader>
nmap <leader>l :set list!<CR>
nmap <leader>v :tabe $MYVIMRC<CR>
nnoremap <leader>r :!%:p
map <silent> <leader>[ :GFiles<CR>
map <silent> <leader>] :Buffers<CR>
nmap <leader>- :Files<CR>
nmap - <Plug>NetrwBrowseUpDir
nmap <silent> - :Ex<CR>

" nmap <CR> o<Esc>
nnoremap <Leader>o o<Esc>
command! Sudow w suda://%
" tnoremap <C-q> <C-\><C-n>
tnoremap <ESC><ESC> <C-\><C-n>
map <C-a> <esc>ggVG<CR>
nnoremap U <C-R>
nnoremap <leader>s :%s//g<Left><Left>
vnoremap <C-c> "+y
map <leader>p "+p

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


map H ^
map L $

nnoremap j gj
nnoremap k gk

nnoremap <Tab> :tabnext<CR>
nnoremap <S-r> :tabnext<CR>
nnoremap <S-Tab> :tabprevious<CR>
nnoremap <S-e> :tabprevious<CR>
nnoremap <leader><BS> <c-^>

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
" inoremap <left> <nop>
" inoremap <right> <nop>

if has("autocmd")
    autocmd FileType vue syntax sync fromstart
    au FileType xml setlocal foldmethod=syntax
	" autocmd bufwritepost init.vim source $MYVIMRC
    au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
    autocmd! FileType fzf tnoremap <buffer> <Esc><Esc> <C-c>
    autocmd BufNewFile,BufRead .envrc set ft=sh
endif

colorscheme base16-solarized-dark
" set background=dark
" set t_Co=256

source ~/vladDotfiles/nvim/.config/nvim/coc.vim
