#!/bin/bash

[[ -f /etc/arch-release ]] && pacmancheck.sh neovim python-neovim fzf ripgrep nodejs yarn
[[ -f /etc/debian_version ]] && sudo apt install -y neovim python3-neovim

[ -f /etc/alpine-release ] && sudo apk add neovim py3-pynvim@testing fzf ripgrep nodejs yarn

cd ~/vladDotfiles/
stow --no-folding -v nvim

nvim --headless +PlugInstall +qa

