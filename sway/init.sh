#!/bin/sh

[[ -f /etc/arch-release ]] && pacmancheck.sh sway swayidle swaylock swaybg fd fzf noto-fonts-emoji ttf-dejavu ttf-droid ttf-hack ttf-liberation sysstat acpi alsa-utils udiskie pulseaudio pulseaudio-bluetooth pulseaudio-alsa perl-file-mimeinfo bluez bluez-utils python-pywal alacritty bemenu kitty xorg-server-xwayland dmenu polkit-gnome awesome-terminal-fonts

[[ -f /etc/debian_version ]] && echo "debian"

[[ -f /etc/alpine-release ]] && echo "alpine"


cd ~/vladDotfiles/
stow --no-folding -v sway 

# wal -i ~/vladDotfiles/_wallpapers/l_int02_big.jpg
