#!/bin/bash

[[ -f /etc/arch-release ]] && sudo pacman -Sy --needed --noconfirm kitty python-pillow
[[ -f /etc/debian_version ]] && echo "debian"

cd ~/vladDotfiles/
stow --no-folding -v kitty
