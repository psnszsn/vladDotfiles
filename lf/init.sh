#!/bin/bash

[[ -f /etc/arch-release ]] && yay -Sy --needed --noconfirm lf perl-image-exiftool highlight
[[ -f /etc/debian_version ]] && echo "debian"
cd ~/vladDotfiles/
stow --no-folding -v lf
