#!/bin/bash


[[ -f /etc/arch-release ]] && pacmancheck.sh tmux
[[ -f /etc/debian_version ]] && sudo apt install -y tmux

cd ~/vladDotfiles/
stow -v tmux
